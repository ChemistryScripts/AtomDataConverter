'''
Created on 12 apr. 2017

@author: jens
'''

import re
from collections import namedtuple

DEBUG = 1
DEBUG_LOG_FILE = 'debug.log'


class Converter:
    
    
    inputFiles = []
    outputFile = ''
    nameFile = ''
    
    dataList = [[]]
    nameList = []
    
    AtomData = namedtuple("AtomData", "resid name")
    
    '''
    Regex:
    https://regex101.com/
    --------
    \s*                match whitespace (none or more)
    \d*\.              match digit or point (none or more)
    [a-zA-Z0-9#]*      match letter, digit or '#' (none or more)
    '''
    
    ATOM_REGEX = r"and resid\s*(\d*)\s*and name\s*([a-zA-Z0-9#]*)"
    ATOM_DATA_REGEX = r"\)\s*([\d\.]+)\s*([\d\.]+)\s*([\d\.]+)"
    ATOM_MATCH_ID_REGEX = r"{\s*(\d*)}"
    
    def __init__(self, inputFiles, nameFile, outputFile):
        self.inputFiles = inputFiles
        self.outputFile = outputFile
        self.nameFile = nameFile
        
        self.parseResidueNameFile()
        
        self.clearOutputFile()
        
        if DEBUG:
            dbg_f = open(DEBUG_LOG_FILE, 'w')
            dbg_f.close()
        
        for inputFile in self.inputFiles:
            print("Parsing ... " + inputFile)
            self.parseInputFile(inputFile)
            self.parseDataCluster()
            
        
    def parseResidueNameFile(self):
        f = open(self.nameFile, 'r')
    
        for line in f:
            self.nameList.append(line.strip())
            
        f.close()
        
    def parseInputFile(self, file):
        f = open(file, 'r')
    
        atomList = []
        self.dataList = []
        
        for line in f:
            if len(line.split()) != 0:
                
                if str.lower(line.split()[0]) == 'assi':
                    if len(atomList) != 0:
                        self.dataList.append(atomList)
                        atomList = []
                        
                    atomList.append(line)
                else:
                    atomList.append(line)
             
        if len(atomList) != 0:
            self.dataList.append(atomList)
            
        f.close()
            
            
    def parseDataCluster(self):
        
        
        if DEBUG:
            dbg_f = open(DEBUG_LOG_FILE, 'a')
            
            
        # Loop over results
        for atomCluster in self.dataList:
            
            matchId = 0
            distanceData = []
            atomCouples = []

            # Loop over lines in data cluster
            for atomDataLine in atomCluster:
                
                if DEBUG:
                    print(atomDataLine)
                    dbg_f.write('\n' + atomDataLine)
                
                # Find atom couples
                matchData = re.findall(self.ATOM_REGEX, atomDataLine)
                
                if (matchData is None) | len(matchData) != 2:
                    print("ERROR (ATOM DATA PARSE (1))")
                elif len(matchData[0]) != 2 | len(matchData[1]) != 2:
                    print("ERROR (ATOM DATA PARSE (2))")

                else:
                    
                    #  Atom data
                    atomCouples.append((self.AtomData(int(matchData[0][0]), matchData[0][1]), self.AtomData(int(matchData[1][0]), matchData[1][1])))


                if str.lower(atomDataLine.split()[0]) == 'assi':

                    # Find match id
                    matchIdData = re.findall(self.ATOM_MATCH_ID_REGEX, atomDataLine)
                    
                    if (matchIdData is not None) & (len(matchIdData) == 1):
                        matchId = int(matchIdData[0])
                    
                    # Find distance data
                    matchAtomData = re.findall(self.ATOM_DATA_REGEX, atomDataLine)
                    
                    if (matchAtomData is not None) & (len(matchAtomData) == 1) & (len(matchAtomData[0]) == 3):
                        # Atom distance data
                        distanceData.append(float(matchAtomData[0][0]))
                        distanceData.append(float(matchAtomData[0][1]))
                        distanceData.append(float(matchAtomData[0][1]))
                        


                
                
            # Generate output
            outputLine = ''
            if len(atomCouples) == 1:
                
                #######################
                #     Unambig data    #
                #######################
                for atomCoupleData in atomCouples:
                    
                    #  First atom data
                    outputLine = outputLine + str(atomCoupleData[0].resid) + ' ' + self.nameList[atomCoupleData[0].resid -1] + ' ' + atomCoupleData[0].name
                    
                    # Second atom data
                    outputLine = outputLine + ' ' + str(atomCoupleData[1].resid) + ' ' + self.nameList[atomCoupleData[1].resid -1] + ' ' + atomCoupleData[1].name
    
                    # Atom distance data
                    outputLine = outputLine + ' ' + format(distanceData[0] - distanceData[1], '.3f')
                    outputLine = outputLine + ' ' + format(distanceData[0] + distanceData[2], '.3f')
    
                    outputLine = outputLine + '\n'
                
                    self.writeOutput(outputLine)
            
                if DEBUG:
                    print(outputLine)
                    
                    dbg_f.write(outputLine)
                    
            else:
            
                #####################
                #     Ambig data    #
                #####################
                
                self.processAmbigueData(atomCouples, matchId)
                    
        if DEBUG:
            dbg_f.close()
            
    def processAmbigueData(self, atomCouplesList, matchId):
        
        atomNameList = []
        atomNameCount = []
        indexCouplesList = []
        
        for atomCoupleData in atomCouplesList:
            indexCouple = [] 
            for atom in atomCoupleData:
                
                found = 0
                idx = 0
                for atomInList in atomNameList:
                    if (atomInList.resid == atom.resid) & (atomInList.name == atom.name):
                        found = 1
                        indexCouple.append(idx)
                        atomNameCount[idx] = atomNameCount[idx] + 1 
                        break
                    idx = idx + 1
                    
                if found == 0:
                    
                    atomNameList.append(self.AtomData(atom.resid,atom.name))
                    atomNameCount.append(1)
                    indexCouple.append(len(atomNameCount) - 1)
            
            indexCouplesList.append((indexCouple[0],indexCouple[1]))
            
        '''
        atomNameList: List of atom data (resid and name)
        atomNameCount: List with number of times that atom is counted
        indexCouplesList: Couple data with indexes
        '''
        if DEBUG:          
            print(atomNameList)
            print(atomNameCount)
            print(indexCouplesList)
            print('\n')
        
        
                   
    def clearOutputFile(self):  
        #clear file
        f = open(self.outputFile, 'w')
        f.close()
        
        
    def writeOutput(self, data):
        
        #clear file
        f = open(self.outputFile, 'a')
        
        f.write(data)
                
        f.close()
                

if __name__ == '__main__':
    
    atomFiles = [
            'unambig.tbl',
            # 'ambig.tbl'
            ]
    
    conv = Converter(atomFiles, 'residue.txt', 'DIST.txt')
    
